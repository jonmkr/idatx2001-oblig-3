package idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.stream.Collectors;

/**
 * A collection of cards with various methods for retrieving hand states
 */
public class HandOfCards {
    private ArrayList<PlayingCard> cards;

    public HandOfCards(ArrayList<PlayingCard> cards) {
        this.cards = cards;
    }

    /**
     * Evaluates whether or not the current hand contains a flush, 5 cards of equal suit
     * @return true if the hand is flush, false if not
     */
    public boolean isFlush() {
        return this.cards.stream().collect(Collectors.groupingBy(PlayingCard::getSuit, Collectors.counting())).containsValue(5L);
    }

    /**
     * Calculates the sum of the value or faces of the current hand. Aces count as 1
     * @return the integer sum of the faces
     */

    public int sumOfFaces() {
        return this.cards.stream().map(PlayingCard::getFace).reduce(0, Integer::sum);
    }

    /**
     * Generates a string of every playing card in the current hand of the heart suit
     * @return a string with the string representations of every card of hearts
     */
    public String cardsOfHeart() {
        return this.cards.stream().filter((card -> card.getSuit() == 'H')).map(PlayingCard::getAsString).collect(Collectors.joining(" "));
    }


    /**
     * Evaluates whether or the current hand contains a queen of spades
     * @return true if hand contains queen of spades, false if it does not
     */
    public boolean hasQueenOfSpades() {
        return this.cards.stream().map(PlayingCard::getAsString).collect(Collectors.toList()).contains("S12");
    }

    public ArrayList<PlayingCard> getCards() {
        return this.cards;
    }

    @Override
    public String toString() {
        return cards.toString();
    }
}
