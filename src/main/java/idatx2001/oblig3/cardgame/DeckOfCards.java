package idatx2001.oblig3.cardgame;

import java.util.ArrayList;
import java.util.Random;


public class DeckOfCards {
    private final char[] suits = {'S', 'H', 'D', 'C'};
    private final ArrayList<PlayingCard> deck;
    private final Random random = new Random();

    /**
     * Generates a deck of 52 playing cards
     */
    public DeckOfCards() {
        this.deck = new ArrayList<PlayingCard>();

        for (char suit: suits) {
            for (int j = 1; j <= 13; j++) {
                this.deck.add(new PlayingCard(suit, j));
            }
        }
    }

    /**
     * Creates a collection of randomly drawn playing cards
     * @param n the number of cards to deal
     * @return a new HandOfCards object with the randomized cards
     */

    public HandOfCards dealHand(int n) {
        PlayingCard card;
        ArrayList<PlayingCard> drawn = new ArrayList<PlayingCard>();
        for (int i = 0; i < n; i++) {
             card = this.deck.get(random.nextInt(this.deck.size()));
             if (drawn.contains(card)) {
                 i--;
            } else {
                 drawn.add(card);
             }
        }
        return new HandOfCards(drawn);
    }

    public ArrayList<PlayingCard> getDeck() {
        return this.deck;
    }
}
