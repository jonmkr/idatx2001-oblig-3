package idatx2001.oblig3.graphics;


import idatx2001.oblig3.cardgame.DeckOfCards;
import idatx2001.oblig3.cardgame.HandOfCards;
import idatx2001.oblig3.cardgame.PlayingCard;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

import java.net.URL;
import java.util.ResourceBundle;


public class Controller implements Initializable{
    @FXML ImageView card1;
    @FXML ImageView card2;
    @FXML ImageView card3;
    @FXML ImageView card4;
    @FXML ImageView card5;

    @FXML Label sumOfFaces;
    @FXML Label cardOfHearts;
    @FXML Label flush;
    @FXML Label queenOfSpades;

    ImageView[] cardArray;
    DeckOfCards deck;
    HandOfCards hand;

    public Controller(){
        deck = new DeckOfCards();
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        cardArray = new ImageView[]{card1, card2, card3, card4, card5};
    }

    @FXML
    void onDealHand(ActionEvent event) {
        hand = deck.dealHand(5);
        for (int i = 0; i < 5; i++) {
            ImageView cardView = cardArray[i];
            PlayingCard card = hand.getCards().get(i);
            Image image = new Image(this.getClass().getResourceAsStream("/playingcards/" + card.getAsString() + ".png"));
            cardView.setImage(image);
        }
    }

    @FXML
    void onCheckHand(ActionEvent event) {
        if (hand != null) {
            sumOfFaces.setText(String.valueOf(hand.sumOfFaces()));
            cardOfHearts.setText(hand.cardsOfHeart());
            flush.setText(hand.isFlush() ? "Yes" : "No");
            queenOfSpades.setText(hand.hasQueenOfSpades() ? "Yes": "No");
        }
    }
}
