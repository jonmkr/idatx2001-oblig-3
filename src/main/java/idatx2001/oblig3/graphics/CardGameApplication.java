package idatx2001.oblig3.graphics;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.net.URL;

public class CardGameApplication extends Application {

    @Override
    public void start(Stage stage) throws Exception {
        URL location = getClass().getResource("/oblig3.fxml");
        FXMLLoader fxmlLoader = new FXMLLoader(location);
        fxmlLoader.setController(new Controller());
        Parent parent = fxmlLoader.load();
        stage.setScene(new Scene(parent));
        stage.show();
    }

    public static void main(String[] args) {
        System.out.println(System.getProperty("user.dir"));
        launch();
    }
}
